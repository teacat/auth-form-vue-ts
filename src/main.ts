import { createApp } from 'vue'
import App from './App.vue'
import './main.scss'
// @ts-ignore
import DisableAutocomplete from 'vue-disable-autocomplete'

const app = createApp(App)

app.use(DisableAutocomplete)
app.mount('#app')
