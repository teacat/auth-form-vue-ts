import { ref } from 'vue';

export function useFetch(url: string, options: RequestInit | undefined) {
    const response = ref();

    const request = async () => {
        const res = await fetch(url, options);
        response.value = await res.json();
    }

    return { response, request }
}
