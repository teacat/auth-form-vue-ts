import { Ref, ref } from 'vue';
import { useFetch } from '@/use/fetch';
import { IUser } from '@/types';

export async function useUsers(): Promise<Ref<IUser>> {
    const loaded = ref<boolean>(false);
    const { response: users, request } = useFetch('https://jsonplaceholder.typicode.com/users', {});

    if (!loaded.value) {
        await request();
        loaded.value = true;
    }

    return users;
}
