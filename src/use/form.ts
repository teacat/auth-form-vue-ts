import {reactive} from 'vue';
import { IForm, IValidField, IValidForm } from '@/types';
import { useField } from '@/use/field';

export function useForm(init: IForm): IValidForm {
    const form = reactive({}) as {[index: string]: IValidField};

    for (const [key, val] of Object.entries(init)) {
        form[key] = useField(val);
    }

    return form as IValidForm; // <IValidForm>form - not work with JSX
}
