import { reactive, ref, watch } from 'vue';
import { IField, IValidField } from '@/types';

export function useField(field: IField): IValidField {
    const valid = ref<boolean>(true);
    const value = ref<string>(field.value);
    const touched = ref<boolean>(false);
    const errors = reactive({}) as { [index: string]: boolean };

    const reAssign = (val: string) => {
        Object.keys(field.validators ?? {}).map(name => {
            const isValid = field.validators[name](val);
            errors[name] = !isValid;
            valid.value = isValid;
        });
    }

    const blur = (): void => {
        touched.value = true;
    }

    watch(value, reAssign, { immediate: true });

    return { value, valid, touched, errors, blur };
}
