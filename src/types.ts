import type { Ref } from 'vue';

export interface IField {
    value: string;
    validators: any;
}

export interface IValidField {
    value: Ref<string>;
    valid: Ref<boolean>;
    touched: Ref<boolean>;
    errors: {
        [index: string]: boolean;
    };
    blur: Function;
}

export interface IForm {
    email: IField;
    password: IField;
}

export interface IValidForm {
    email: IValidField;
    password: IValidField;
    valid: boolean;
    [index: string]: IValidField | boolean;
}

export interface IUser {
    id: number;
    name: string;
}
